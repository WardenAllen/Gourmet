//
//  OrderInfo+CoreDataProperties.m
//  
//
//  Created by WardenAllen on 15/12/25.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OrderInfo+CoreDataProperties.h"

@implementation OrderInfo (CoreDataProperties)

@dynamic couponType;
@dynamic orderContent;
@dynamic orderId;
@dynamic orderOffPrice;
@dynamic orderRealPrice;
@dynamic orderState;
@dynamic orderTime;
@dynamic orderTotalPrice;
@dynamic paymentType;
@dynamic restaurantId;
@dynamic restaurantName;
@dynamic userId;
@dynamic orderRemark;

@end
