//
//  CartButton.m
//  Gourmet
//
//  Created by WardenAllen on 15/12/23.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "CartButton.h"

#define BADGE_VIEW_WIDTH 16

@interface CartButton ()

@property (nonatomic, strong) UILabel *badgeLabel;

@end

@implementation CartButton

- (instancetype)init {
    if ( self = [super init] ) {
        [self setUpWithBadgeNumber:0];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder]) {
        [self setUpWithBadgeNumber:0];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        [self setUpWithBadgeNumber:0];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame badgeNumber:(NSInteger)badgeNumber {
    if (self = [super initWithFrame:frame]) {
        [self setUpWithBadgeNumber:badgeNumber];
    }
    return self;
}

- (void)setUpWithBadgeNumber:(NSInteger)badgeNumber {
    if (badgeNumber > 0) {
        self.badgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-BADGE_VIEW_WIDTH/2, -BADGE_VIEW_WIDTH/2, BADGE_VIEW_WIDTH, BADGE_VIEW_WIDTH)];
        self.badgeLabel.backgroundColor = [UIColor redColor];
        self.badgeLabel.text = [NSString stringWithFormat:@"%ld", (long)badgeNumber];
        self.badgeLabel.textColor = [UIColor whiteColor];
        self.badgeLabel.textAlignment = NSTextAlignmentCenter;
        if (badgeNumber < 10) {
            self.badgeLabel.font = [UIFont boldSystemFontOfSize:13];
        } else if (badgeNumber>=10 && badgeNumber<100) {
            self.badgeLabel.font = [UIFont boldSystemFontOfSize:11];
        } else {
            self.badgeLabel.font = [UIFont boldSystemFontOfSize:9];
        }
        CALayer *layer = [self.badgeLabel layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:BADGE_VIEW_WIDTH/2];
        [self addSubview:self.badgeLabel];
    }
}

- (void)setBadgeNumber:(NSInteger)badgeNumber {
    if (badgeNumber > 0) {
        if (self.badgeLabel) {
            if (badgeNumber < 10) {
                self.badgeLabel.font = [UIFont boldSystemFontOfSize:13];
            } else if (badgeNumber>=10 && badgeNumber<100) {
                self.badgeLabel.font = [UIFont boldSystemFontOfSize:11];
            } else {
                self.badgeLabel.font = [UIFont boldSystemFontOfSize:9];
            }
            self.badgeLabel.text = [NSString stringWithFormat:@"%ld", (long)badgeNumber];
        } else {
            self.badgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-BADGE_VIEW_WIDTH/2, -BADGE_VIEW_WIDTH/2, BADGE_VIEW_WIDTH, BADGE_VIEW_WIDTH)];
            self.badgeLabel.backgroundColor = [UIColor redColor];
            self.badgeLabel.text = [NSString stringWithFormat:@"%ld", (long)badgeNumber];
            self.badgeLabel.textColor = [UIColor whiteColor];
            self.badgeLabel.textAlignment = NSTextAlignmentCenter;
            if (badgeNumber < 10) {
                self.badgeLabel.font = [UIFont boldSystemFontOfSize:13];
            } else if (badgeNumber>=10 && badgeNumber<100) {
                self.badgeLabel.font = [UIFont boldSystemFontOfSize:11];
            } else {
                self.badgeLabel.font = [UIFont boldSystemFontOfSize:9];
            }
            CALayer *layer = [self.badgeLabel layer];
            [layer setMasksToBounds:YES];
            [layer setCornerRadius:BADGE_VIEW_WIDTH/2];
            [self addSubview:self.badgeLabel];
        }
    } else {
        if (self.badgeLabel) {
            [self.badgeLabel removeFromSuperview];
            self.badgeLabel = nil;
        }
    }
}

- (NSInteger)badgeNumber {
    if (self.badgeLabel) {
        return [self.badgeLabel.text integerValue];
    } else {
        return 0;
    }
}

@end
