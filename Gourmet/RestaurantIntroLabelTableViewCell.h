//
//  RestaurantIntroLabelTableViewCell.h
//  Gourmet
//
//  Created by WardenAllen on 15/12/21.
//  Copyright © 2015年 王东. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantIntroLabelTableViewCell : UITableViewCell
- (void)setUpWithIntroName:(NSString *)name;

@end
