//
//  RestaurantDetailTableViewController.m
//  Gourmet
//
//  Created by WardenAllen on 15/12/3.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "RestaurantDetailTableViewController.h"
#import "RestaurantBasicInfoTableViewCell.h"
#import "RestaurantAddressTableViewCell.h"
#import "RestaurantIntroLabelTableViewCell.h"
#import "AppDelegate.h"
#import "OrderViewController.h"

#define kCount 6
#define kScrollHeight 160

@interface RestaurantDetailTableViewController ()<UIScrollViewDelegate> {
    UIScrollView *_scrollView;
    //    NSMutableArray *slideImages;
    UIPageControl *_pageControl;
}

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (assign, nonatomic) int currentPage;
@property (strong) NSMutableArray *logoArray;

@end

@implementation RestaurantDetailTableViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"RestaurantID is %ld", (long)self.restaurantId);
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kWIDTH, kScrollHeight*kWIDTH_RATIO)];
    for (int i = 0; i< kCount; i++) {
        UIImageView *imageView = [[UIImageView alloc] init];
        
        // 1.设置frame
        imageView.frame = CGRectMake(kWIDTH*i, 0, kWIDTH, kScrollHeight*kWIDTH_RATIO);
        
        // 2.设置图片
        NSString *imgName = [NSString stringWithFormat:@"%d.jpg", i + 1];
        imageView.image = [UIImage imageNamed:imgName];
        
        [_scrollView addSubview:imageView];
    }
    
    // height == 0 代表 禁止垂直方向滚动
    _scrollView.contentSize = CGSizeMake(kCount * kWIDTH, 0);
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.pagingEnabled = YES;
    _scrollView.delegate = self;
    _scrollView.tag = 1; // 与TableView的ScrollView区分开
    
    [self.headerView addSubview:_scrollView];
    
//    // 添加PageControl
//    UIPageControl *pageControl = [[UIPageControl alloc] init];
//    pageControl.center = CGPointMake(kWIDTH * 0.5, kScrollHeight*kWIDTH_RATIO - 20);
//    pageControl.bounds = CGRectMake(0, 0, 150, 50);
//    pageControl.numberOfPages = kCount; // 一共显示多少个圆点（多少页）
//    // 设置非选中页的圆点颜色
//    pageControl.pageIndicatorTintColor = [UIColor yellowColor];
//    // 设置选中页的圆点颜色
//    pageControl.currentPageIndicatorTintColor = [UIColor greenColor];
//    
//    // 禁止默认的点击功能
//    pageControl.enabled = NO;
//    
//    [self.headerView addSubview:pageControl];
//    _pageControl = pageControl;
    

    NSLog(@"%@", self.restaurantName);
    NSLog(@"%f", self.rank);
    NSLog(@"%ld", (long)self.rankCount);
    
    //[self.tabBarController.tabBar setHidden:YES];
//    [UIView animateWithDuration:0.3 animations:^(){
//        [self.tabBarController.tabBar setFrame:CGRectMake(-kWIDTH, kHEIGHT-48, kWIDTH_RATIO, 48)];
//    } completion:^(BOOL finished) {
//        //[self.tabBarController.tabBar setHidden:YES];
//    }];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, kHEIGHT-40*kWIDTH_RATIO- 60, kWIDTH, 40*kWIDTH_RATIO)];
    button.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.7];
    [button setTitle:@"开始点餐" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(orderButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view insertSubview:button aboveSubview:self.tableView];

}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
////    [self.tabBarController.tabBar setHidden:YES];
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    [self.tabBarController.tabBar setHidden:NO];
//    [self.tabBarController.tabBar setFrame:CGRectMake(0, kHEIGHT-48, kWIDTH_RATIO, 48)];
//    [UIView animateWithDuration:0.35 animations:^(){
//        [self.tabBarController.tabBar setHidden:NO];
//        [self.tabBarController.tabBar setFrame:CGRectMake(0, kHEIGHT-48, kWIDTH_RATIO, 48)];
//    } completion:^(BOOL finished) {
//    }];
//}

#pragma mark - Actions

- (void)orderButtonClicked:(id)sender {
    NSLog(@"Button Clicked");
    
    OrderViewController *orderViewController = (OrderViewController *)[[AppDelegate globalDelegate] OrderViewController];
    NSDictionary *restaurantInfo = @{@"RestaurantId":[NSString stringWithFormat:@"%ld", (long)self.restaurantId], @"RestaurantName":self.restaurantName};
    orderViewController.restaurantInfo = restaurantInfo;
    [self.navigationController pushViewController:orderViewController animated:YES];
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.tag == 1) {
        self.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
        NSLog(@"%d", self.currentPage);
        // 设置页码
        _pageControl.currentPage = self.currentPage;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return kScrollHeight*(kWIDTH_RATIO-1);
            break;
            
        default:
            break;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
            case 0:             //名称＋评分
                return 50*kWIDTH_RATIO;
                break;
                
            case 1:             //地址
                return 30*kWIDTH_RATIO;
                break;
                
            default:
                break;
        }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.row) {
        case 0:
        {
            RestaurantBasicInfoTableViewCell *basicInfoCell = [tableView dequeueReusableCellWithIdentifier:@"BasicInfoCell"];
            basicInfoCell.selectionStyle = UITableViewCellSelectionStyleNone;
            basicInfoCell.nameLabel.text = self.restaurantName;
            basicInfoCell.rank = self.rank;
            basicInfoCell.rankCount = self.rankCount;
            return basicInfoCell;
        }
            break;
            
        case 1:
        {
            RestaurantAddressTableViewCell *addressCell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
            [addressCell setUpWithAddress:@"Test Address"];
            
            return addressCell;
        }
            break;
            
        default:
            break;
    }
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
