//
//  RestaurantBasicInfoTableViewCell.m
//  Gourmet
//
//  Created by WardenAllen on 15/12/7.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "RestaurantBasicInfoTableViewCell.h"
#import "RankView.h"

#define RankViewWidth 100
#define RankViewHeight 12

#define RankCountLabelWidth 28

@interface RestaurantBasicInfoTableViewCell ()

@property (nonatomic, strong) RankView *rankView;
@property (nonatomic, strong) UILabel *rankCountLabel;

@end

@implementation RestaurantBasicInfoTableViewCell

- (void)awakeFromNib {
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(8*kWIDTH_RATIO, 8*kWIDTH_RATIO, kWIDTH-8*kWIDTH_RATIO*2, 18)];
    //self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.font = [UIFont systemFontOfSize:18.0];
    [self addSubview:self.nameLabel];
    
    self.rankView = [[RankView alloc] initWithFrame:CGRectMake(8*kWIDTH_RATIO, (8+18+4)*kWIDTH_RATIO, RankViewWidth*kWIDTH_RATIO, RankViewHeight*kWIDTH_RATIO)];
    [self addSubview:self.rankView];
    
    self.rankCountLabel = [[UILabel alloc] initWithFrame:CGRectMake((8+RankViewWidth+8)*kWIDTH_RATIO, (8+18+4)*kWIDTH_RATIO, RankCountLabelWidth*kWIDTH_RATIO, RankViewHeight*kWIDTH_RATIO)];
    self.rankCountLabel.font = [UIFont systemFontOfSize:14.0];
    [self addSubview:self.rankCountLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setRank:(float)rank {
    if (rank > 10.0) {
        rank = 10.0;
    } else if (rank < 0) {
        rank = 0;
    }
    self.rankView.rank = rank;
}

- (void)setRankCount:(NSInteger)rankCount {
    self.rankCountLabel.text = [NSString stringWithFormat:@"%ld", (long)rankCount];
}

@end
