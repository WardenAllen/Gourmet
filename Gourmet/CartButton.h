//
//  CartButton.h
//  Gourmet
//
//  Created by WardenAllen on 15/12/23.
//  Copyright © 2015年 王东. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartButton : UIButton

@property (nonatomic, assign) NSInteger badgeNumber;

- (instancetype) initWithFrame:(CGRect)frame badgeNumber:(NSInteger)badgeNumber;

@end
