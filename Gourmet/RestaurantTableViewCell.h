//
//  RestaurantTableViewCell.h
//  Gourmet
//
//  Created by WardenAllen on 15/11/15.
//  Copyright © 2015年 王东. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
