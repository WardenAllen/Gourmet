//
//  OrderInfo+CoreDataProperties.h
//  
//
//  Created by WardenAllen on 15/12/25.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OrderInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderInfo (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *couponType;
@property (nullable, nonatomic, retain) NSData *orderContent;
@property (nullable, nonatomic, retain) NSNumber *orderId;
@property (nullable, nonatomic, retain) NSNumber *orderOffPrice;
@property (nullable, nonatomic, retain) NSNumber *orderRealPrice;
@property (nullable, nonatomic, retain) NSNumber *orderState;
@property (nullable, nonatomic, retain) NSDate *orderTime;
@property (nullable, nonatomic, retain) NSNumber *orderTotalPrice;
@property (nullable, nonatomic, retain) NSNumber *paymentType;
@property (nullable, nonatomic, retain) NSNumber *restaurantId;
@property (nullable, nonatomic, retain) NSString *restaurantName;
@property (nullable, nonatomic, retain) NSNumber *userId;
@property (nullable, nonatomic, retain) NSString *orderRemark;

@end

NS_ASSUME_NONNULL_END
