//
//  CartTableViewCell.m
//  Gourmet
//
//  Created by WardenAllen on 15/12/25.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "CartTableViewCell.h"

@interface CartTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *dishNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dishCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dishPriceLabel;

@property (weak, nonatomic) IBOutlet UIButton *removeFromCartBtn;
@property (weak, nonatomic) IBOutlet UIButton *addToCartBtn;

@end

@implementation CartTableViewCell

#pragma mark - Life Cycle

- (void)awakeFromNib {
    CALayer *rLayer = [self.removeFromCartBtn layer];
    [rLayer setMasksToBounds:YES];
    [rLayer setCornerRadius:self.removeFromCartBtn.frame.size.width/2];
    
    CALayer *aLayer = [self.addToCartBtn layer];
    [aLayer setMasksToBounds:YES];
    [aLayer setCornerRadius:self.addToCartBtn.frame.size.width/2];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUpWithDishName:(NSString *)dishName dishCount:(NSInteger)dishCount dishPrice:(float)dishPrice {
    self.dishNameLabel.text = dishName;
    self.dishCountLabel.text = [NSString stringWithFormat:@"%ld", (long)dishCount];
    self.dishPriceLabel.text = [NSString stringWithFormat:@"%.1f" , dishPrice];
}

#pragma mark - Actions

- (IBAction)removeFromCartBtnClicked:(id)sender {
    NSInteger currentDishCount = [self.dishCountLabel.text integerValue];
    currentDishCount -= 1;
    if (currentDishCount <= 0) {
        currentDishCount = 0;
    } else {
        self.dishCountLabel.text = [NSString stringWithFormat:@"%ld", (long)currentDishCount];
    }
    if ([self.delegate respondsToSelector:@selector(removeFromCartBtnClicked:withCurrentDishCount:)]) {
        [self.delegate removeFromCartBtnClicked:self withCurrentDishCount:currentDishCount];
    }
}

- (IBAction)addToCartBtnClicked:(id)sender {
    NSInteger currentDishCount = [self.dishCountLabel.text integerValue];
    currentDishCount += 1;
    self.dishCountLabel.text = [NSString stringWithFormat:@"%ld", (long)currentDishCount];
    if ([self.delegate respondsToSelector:@selector(addToCartBtnClicked:withCurrentDishCount:)]) {
        [self.delegate addToCartBtnClicked:self withCurrentDishCount:currentDishCount];
    }
}

@end
