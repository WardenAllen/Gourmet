//
//  RestaurantAddressTableViewCell.m
//  Gourmet
//
//  Created by WardenAllen on 15/12/21.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "RestaurantAddressTableViewCell.h"

@interface RestaurantAddressTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@end

@implementation RestaurantAddressTableViewCell

- (void)setUpWithAddress:(NSString *)address {
    self.addressLabel.text = address;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
