//
//  RestaurantTableViewCell.m
//  Gourmet
//
//  Created by WardenAllen on 15/11/9.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell

- (void)awakeFromNib {
    CALayer *layer = [_logoImageView layer];
    [layer setMasksToBounds:YES];
    [layer setCornerRadius:4.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
