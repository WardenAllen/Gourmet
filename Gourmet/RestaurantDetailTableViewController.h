//
//  RestaurantDetailTableViewController.h
//  Gourmet
//
//  Created by WardenAllen on 15/12/3.
//  Copyright © 2015年 王东. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantDetailTableViewController : UITableViewController

@property (nonatomic, assign) NSInteger restaurantId;
@property (nonatomic, strong) NSString *restaurantName;
@property (nonatomic, assign) float     rank;
@property (nonatomic, assign) NSInteger rankCount;

@end
