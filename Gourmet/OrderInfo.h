//
//  OrderInfo.h
//  
//
//  Created by WardenAllen on 15/12/25.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderInfo : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "OrderInfo+CoreDataProperties.h"
