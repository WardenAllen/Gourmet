//
//  CartTableViewCell.h
//  Gourmet
//
//  Created by WardenAllen on 15/12/25.
//  Copyright © 2015年 王东. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CartTableViewCell;

@protocol CartCellBtnDelegate <NSObject>

- (void)removeFromCartBtnClicked:(CartTableViewCell *)cartCell withCurrentDishCount:(NSInteger)currentDishCount;
- (void)addToCartBtnClicked:     (CartTableViewCell *)cartCell withCurrentDishCount:(NSInteger)currentDishCount;

@end

@interface CartTableViewCell : UITableViewCell

@property (nonatomic, weak) id<CartCellBtnDelegate>delegate;
- (void)setUpWithDishName:(NSString *)dishName dishCount:(NSInteger)dishCount dishPrice:(float)dishPrice;

@end
