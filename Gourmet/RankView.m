//
//  RankView.m
//  Gourmet
//
//  Created by WardenAllen on 15/12/7.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "RankView.h"

#define totalRank 10.0

@interface RankView ()

@property (nonatomic, strong) UIView *foregroundStarView;
@property (nonatomic, strong) UIView *backgroundStarView;

@end

@implementation RankView

- (instancetype)init {
    if ( self = [super init] ) {
        [self setUpWithRank:0];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder]) {
        [self setUpWithRank:0];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        [self setUpWithRank:0];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame rank:(float)rank {
    if (self = [super initWithFrame:frame]) {
        [self setUpWithRank:rank];
    }
    return self;
}

- (void)setUpWithRank:(float)rank {
    self.foregroundStarView = [self createStarViewWithImageName:@"YellowStar.png"];
    self.backgroundStarView = [self createStarViewWithImageName:@"GrayStar.png"];
    
    [self addSubview:self.backgroundStarView];
    [self addSubview:self.foregroundStarView];
    
    self.foregroundStarView.frame = CGRectMake(0, 0, self.bounds.size.width * rank / totalRank, self.bounds.size.height);
}

- (UIView *)createStarViewWithImageName:(NSString *)imageName {
    UIView *view = [[UIView alloc] initWithFrame:self.bounds];
    view.clipsToBounds = YES;
    view.backgroundColor = [UIColor clearColor];
    for (NSInteger i = 0; i < 5; i ++)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        imageView.frame = CGRectMake(i * self.bounds.size.width / 5, 0, self.bounds.size.width / 5, self.bounds.size.height);
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [view addSubview:imageView];
    }
    return view;
}

- (void)setRank:(CGFloat)rank {
    self.foregroundStarView.frame = CGRectMake(0, 0, self.bounds.size.width * rank / totalRank, self.bounds.size.height);
}

@end
