//
//  OrderViewController.h
//  Gourmet
//
//  Created by WardenAllen on 15/12/22.
//  Copyright © 2015年 王东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVOSCloud/AVOSCloud.h>

@interface OrderViewController : UIViewController
@property (nonatomic, strong) NSDictionary *restaurantInfo;
@end
