//
//  AppDelegate.h
//  Gourmet
//
//  Created by WardenAllen on 15/10/8.
//  Copyright © 2015年 王东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) UITableViewController *RestaurantDetailTableViewController;
@property (nonatomic, strong) UIViewController      *OrderViewController;
@property (nonatomic, strong) UITableViewController *CartTableViewController;
@property (nonatomic, strong) UITableViewController *PaymentTableViewController;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

#pragma mark - public
+ (AppDelegate *)globalDelegate;

@end

