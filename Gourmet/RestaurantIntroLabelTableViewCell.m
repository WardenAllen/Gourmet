//
//  RestaurantIntroLabelTableViewCell.m
//  Gourmet
//
//  Created by WardenAllen on 15/12/21.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "RestaurantIntroLabelTableViewCell.h"

@interface RestaurantIntroLabelTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *introLabel;

@end

@implementation RestaurantIntroLabelTableViewCell

- (void)setUpWithIntroName:(NSString *)name {
    self.introLabel.text = name;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
