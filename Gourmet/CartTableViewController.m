//
//  CartTableViewController.m
//  Gourmet
//
//  Created by WardenAllen on 15/12/25.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "CartTableViewController.h"
#import "CartTableViewCell.h"
#import "AppDelegate.h"
#import "PaymentTableViewController.h"

#define CHECKOUT_BTN_HEIGHT 48

@interface CartTableViewController () <CartCellBtnDelegate>

@end

@implementation CartTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    UIButton *checkoutBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, kHEIGHT-CHECKOUT_BTN_HEIGHT*kWIDTH_RATIO-64, kWIDTH, CHECKOUT_BTN_HEIGHT*kWIDTH_RATIO)];
    checkoutBtn.backgroundColor = [UIColor colorWithRed:1.0 green:0 blue:0 alpha:0.6];
    [checkoutBtn setTitle:@"结账" forState:UIControlStateNormal];
    [checkoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [checkoutBtn addTarget:self action:@selector(checkBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view insertSubview:checkoutBtn aboveSubview:self.tableView];
}

#pragma mark - Actions

- (void)checkBtnClicked:(id)sender {
    PaymentTableViewController *ptvc = (PaymentTableViewController *)[[AppDelegate globalDelegate] PaymentTableViewController];
    ptvc.orderInfo = self.orderInfo;
    
    [self.navigationController pushViewController:ptvc animated:YES];
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *orderArray = [NSKeyedUnarchiver unarchiveObjectWithData:self.orderInfo.orderContent];
    return orderArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *orderArray = [NSKeyedUnarchiver unarchiveObjectWithData:self.orderInfo.orderContent];
    NSDictionary *orderDic = [orderArray objectAtIndex:indexPath.row];
    NSString *dishName  = [orderDic objectForKey:@"DishName"];
    NSInteger dishCount = [[orderDic objectForKey:@"DishCount"] integerValue];
    float     dishPrice = [[orderDic objectForKey:@"DishPrice"] floatValue];
    
    CartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CartCell"];
    [cell setUpWithDishName:dishName dishCount:dishCount dishPrice:dishPrice];
    cell.delegate = self;
    return cell;
}

#pragma mark - CartCellBtnDelegate

- (void)removeFromCartBtnClicked:(CartTableViewCell *)cartCell withCurrentDishCount:(NSInteger)currentDishCount{
    NSIndexPath *currentCellIndexPath = [self.tableView indexPathForCell:cartCell];
    
    NSMutableArray *orderArray = [[NSKeyedUnarchiver unarchiveObjectWithData:self.orderInfo.orderContent] mutableCopy];
    NSMutableDictionary *orderDic = [[orderArray objectAtIndex:currentCellIndexPath.row] mutableCopy];

    if (currentDishCount == 0) {
        [orderArray removeObjectAtIndex:currentCellIndexPath.row];
        [self.orderInfo setOrderContent:[NSKeyedArchiver archivedDataWithRootObject:orderArray]];
        [self.tableView deleteRowsAtIndexPaths:@[currentCellIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else {
        [orderDic setObject:[NSString stringWithFormat:@"%ld", (long)currentDishCount] forKey:@"DishCount"];
        [self.orderInfo setOrderContent:[NSKeyedArchiver archivedDataWithRootObject:orderArray]];
    }
}

- (void)addToCartBtnClicked:(CartTableViewCell *)cartCell withCurrentDishCount:(NSInteger)currentDishCount{
    NSIndexPath *currentCellIndexPath = [self.tableView indexPathForCell:cartCell];
    
    NSMutableArray *orderArray = [[NSKeyedUnarchiver unarchiveObjectWithData:self.orderInfo.orderContent] mutableCopy];
    NSMutableDictionary *orderDic = [[orderArray objectAtIndex:currentCellIndexPath.row] mutableCopy];
    [orderDic setObject:[NSString stringWithFormat:@"%ld", (long)currentDishCount] forKey:@"DishCount"];
    [self.orderInfo setOrderContent:[NSKeyedArchiver archivedDataWithRootObject:orderArray]];
}

@end
