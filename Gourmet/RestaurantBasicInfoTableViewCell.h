//
//  RestaurantBasicInfoTableViewCell.h
//  Gourmet
//
//  Created by WardenAllen on 15/12/7.
//  Copyright © 2015年 王东. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantBasicInfoTableViewCell : UITableViewCell

@property (strong, nonatomic) UILabel *nameLabel;
@property (assign, nonatomic) float rank;
@property (assign, nonatomic) NSInteger rankCount;

@end
