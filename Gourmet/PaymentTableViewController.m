//
//  PaymentTableViewController.m
//  Gourmet
//
//  Created by WardenAllen on 15/12/25.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "PaymentTableViewController.h"

@interface PaymentTableViewController ()

@end

@implementation PaymentTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 2;        //2种支付方式
            break;
            
        case 1:
            return 1;        //优惠券
            break;
            
        case 2:
            return 1;        //备注
            break;
            
        case 3:
            return 1;
            break;
            
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentCell"];
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"支付宝";
                    break;
                    
                case 1:
                    cell.textLabel.text = @"微信支付";
                    break;

                default:
                    break;
            }
            break;
            
        case 1:
            cell.textLabel.text = @"优惠券";
            break;
            
        case 2:
            cell.textLabel.text = @"备注";
            break;
            
        case 3:
            cell.textLabel.text = @"付款";
        default:
            break;
    }
    return cell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                {
                    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    UITableViewCell *anotherCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                    anotherCell.accessoryType = UITableViewCellAccessoryNone;
                }
                    break;
                    
                case 1:
                {
                    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    UITableViewCell *anotherCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    anotherCell.accessoryType = UITableViewCellAccessoryNone;
                }
                    break;

                default:
                    break;
            }
            break;
            
        case 1:
            
            break;

        case 2:
            
            break;
            
        case 3:
            
            break;
            
        default:
            break;
    }
}

@end
