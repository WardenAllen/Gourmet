//
//  PaymentTableViewController.h
//  Gourmet
//
//  Created by WardenAllen on 15/12/25.
//  Copyright © 2015年 王东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderInfo.h"

@interface PaymentTableViewController : UITableViewController
@property (nonatomic, strong) OrderInfo *orderInfo;
@end
