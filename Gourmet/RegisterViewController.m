//
//  ViewController.m
//  Gourmet
//
//  Created by WardenAllen on 15/10/8.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *testImageVIew;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //注册（包括手机号）
//    AVUser *user = [AVUser user];
//    user.username = @"王东";
//    user.password =  @"wd,19931227.";
//    user.email = @"314917760@qq.com";
//    user.mobilePhoneNumber = @"18721926536";
//    
//    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        if (succeeded) {
//            NSLog(@"Sign Succeed!!");
//        } else {
//            
//        }
//    }];

    //手机号验证
//    [AVUser verifyMobilePhone:@"729361" withBlock:^(BOOL succeeded, NSError *error) {
//        //验证结果
//    }];
    
    
//    AVQuery *query = [AVUser query];
//    [query whereKey:@"email" equalTo:@"314917760@qq.com"];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (error == nil) {
//            if (objects.count > 0) {
//                AVUser *user = (AVUser *)objects.firstObject;
//                NSLog(@"%@", user.username);
//            }
//        } else {
//            
//        }
//    }];

    AVQuery *query = [AVQuery queryWithClassName:@"RestaurantDishInfo"];
    //[query whereKey:@"restaurantId" equalTo:@1];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // 检索成功
            NSLog(@"Successfully retrieved %lu posts.", (unsigned long)objects.count);
            AVObject *restaurantInfo = objects.firstObject;
            AVFile *file = [restaurantInfo objectForKey:@"dishImages"];
            [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                if (!error) {
                    NSLog(@"%@", data);
                } else {
                    NSLog(@"%@ %@", error, [error userInfo]);
                }
            }];
        } else {
            // 输出错误信息
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
}

- (BOOL)isValidateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",emailRegex];
    return [emailTest evaluateWithObject:email];
}

@end
