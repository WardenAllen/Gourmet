//
//  DishInfoView.m
//  Gourmet
//
//  Created by WardenAllen on 15/12/23.
//  Copyright © 2015年 王东. All rights reserved.
//

#import "DishInfoView.h"

#define CHEF_HEAD_IMAGEVIEW_WIDTH 60
#define CHEF_HEADIMAGE_BACKGROUNDVIEW 64

@interface DishInfoView ()
@property (nonatomic, strong) UIImageView *chefHeadImageView;
@end

@implementation DishInfoView

- (instancetype)init {
    if ( self = [super init] ) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder]) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    if (self.frame.size.width!=0 && self.frame.size.height!=0) {
        
        UIView *headImageBackgroundView = [[UIView alloc] initWithFrame:CGRectMake((kWIDTH/2-CHEF_HEADIMAGE_BACKGROUNDVIEW*kWIDTH_RATIO)/2, (10-2)*kWIDTH_RATIO, CHEF_HEADIMAGE_BACKGROUNDVIEW*kWIDTH_RATIO, CHEF_HEADIMAGE_BACKGROUNDVIEW*kWIDTH_RATIO)];
        headImageBackgroundView.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.3];
        CALayer *headImageBackgroundLayer = [headImageBackgroundView layer];
        [headImageBackgroundLayer setMasksToBounds:YES];
        [headImageBackgroundLayer setCornerRadius:CHEF_HEADIMAGE_BACKGROUNDVIEW*kWIDTH_RATIO/2];
        [self addSubview:headImageBackgroundView];
        
        self.chefHeadImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kWIDTH/2-CHEF_HEAD_IMAGEVIEW_WIDTH*kWIDTH_RATIO)/2, 10*kWIDTH_RATIO, CHEF_HEAD_IMAGEVIEW_WIDTH*kWIDTH_RATIO, CHEF_HEAD_IMAGEVIEW_WIDTH*kWIDTH_RATIO)];
        
        self.chefHeadImageView.image = [UIImage imageNamed:@"chef1.png"];
        CALayer *headImageLayer = [self.chefHeadImageView layer];
        [headImageLayer setMasksToBounds:YES];
        [headImageLayer setCornerRadius:CHEF_HEAD_IMAGEVIEW_WIDTH*kWIDTH_RATIO/2];
        [self insertSubview:self.chefHeadImageView aboveSubview:headImageBackgroundView];
        
    }
}

#pragma mark - setter, getter

- (void)setChefHeafImage:(UIImage *)chefHeafImage {
    if (self.chefHeadImageView) {
        self.chefHeadImageView.image = chefHeafImage;
    }
}

@end
