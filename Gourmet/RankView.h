//
//  RankView.h
//  Gourmet
//
//  Created by WardenAllen on 15/12/7.
//  Copyright © 2015年 王东. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RankView : UIView

@property (nonatomic, assign) CGFloat rank;//得分值，范围为0--10，默认为0

- (instancetype)initWithFrame:(CGRect)frame rank:(float)rank;

@end
